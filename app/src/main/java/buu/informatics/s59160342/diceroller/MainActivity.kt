package buu.informatics.s59160342.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    lateinit var  diceImage : ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.roll_button)
        val resetButton: Button = findViewById(R.id.reset_button)

        diceImage = findViewById(R.id.dice_image)

        rollButton.setOnClickListener { rollDice() }
        resetButton.setOnClickListener { Reset() }


    }

    private fun rollDice() {

        Toast.makeText(this,"button clicked",
            Toast.LENGTH_SHORT).show()

        var randomInt = Random.nextInt(6) + 1

        val drawableResource = when(randomInt){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        diceImage.setImageResource(drawableResource)

    }

    private fun Reset() {
        Toast.makeText(this,"button clicked",
            Toast.LENGTH_SHORT).show()

        diceImage.setImageResource(R.drawable.empty_dice)
    }
}
